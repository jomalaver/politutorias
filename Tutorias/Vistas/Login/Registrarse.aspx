﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registrarse.aspx.cs" Inherits="Tutorias.Vistas.Login.Registrarse" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="form-group">
            <asp:Label runat="server" Text="Nombres"></asp:Label>
            <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Apellidos"></asp:Label>
            <asp:TextBox ID="txtApellidos" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Tipo Documento"></asp:Label>
            <asp:DropDownList ID="ddlTipoDocumento" runat="server" CssClass="form-control"></asp:DropDownList>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Numero De Identificacion"></asp:Label>
            <asp:TextBox ID="txtIdentificacion" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Direccion"></asp:Label>
            <asp:TextBox ID="txtDireccion" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Telefono"></asp:Label>
            <asp:TextBox ID="txtTelefono" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Correo "></asp:Label>
            <asp:TextBox ID="txtCorreo" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Fecha Ingreso"></asp:Label>
            <asp:TextBox ID="txtFechaIngreso" runat="server" TextMode="Date" placeholder="12/05/1800" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Ciudad"></asp:Label>
            <asp:DropDownList ID="ddlCiudad" runat="server" CssClass="form-control"></asp:DropDownList>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Rol"></asp:Label>
            <asp:DropDownList ID="ddlRol" runat="server" CssClass="form-control"></asp:DropDownList>
        </div>
        <asp:Panel ID="pnlDisponibilidad" runat="server" Visible="false">

            <div class="form-group">
                <asp:Label runat="server" Text="Disponibilidad"></asp:Label>
                <asp:DropDownList ID="ddlDisponibilidad" runat="server" CssClass="form-control"></asp:DropDownList>
            </div>

        </asp:Panel>
    </form>
</body>
</html>
