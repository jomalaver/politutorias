﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Entrenamiento.VIEWS.Login.Login" %>

<!doctype html>
    <html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Login</title>
        <link href="Bosstrap/StyleSheet.css" rel="stylesheet" />
        <style>
            @import url('https://fonts.googleapis.com/css?family=Berkshire+Swash|Courgette');
        </style>
    </head>
 <body>
     <%--<img src="Images/logopoli_w.png" class="img1" alt="clip" />--%>
     <%--<img src="Images/polit-cnico.jpg" class="img1" alt="clip"/>--%>
 
                <section>
<%--                <form action="Login.php" method="post" runat="server">--%>
             <form runat="server">
                    <div class="container">
                        <div class="div1">
                            <h1>PoliTutorias</h1>
                        </div>
                        <div class="div2">
                            <img src="Images/logopoli_w.png" alt="" />                         
                            <%--<input type="text" name="email" placeholder="email">--%>
                            <asp:TextBox ID="txtEmail" runat="server" placeholder="email"></asp:TextBox>
                            <asp:TextBox ID="txtPassword" runat="server" placeholder="Password"></asp:TextBox>
                            <asp:Button ID="btnIngresar" runat="server" Text="Ingresar" OnClick="btnIngresar_Click"  />
                            <%--<input type="password" name="passwd" placeholder="Password">--%>
                            <span style="color: red"><?php echo $error; ?></span>
                            <a href="#">Forgot Your Password ?</a>
                        </div>
                    </div>
                </form>
            </section>

    </body>
    </html>