﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess.LogicBusiness;

namespace Entrenamiento.VIEWS.Login
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            DataTable dtUsuario = new DataTable();
            string mensaje = string.Empty;
            if (string.IsNullOrEmpty(txtEmail.Text))
            {
                mensaje = "Debe ingresar su correo*";
            }
            if (string.IsNullOrEmpty(txtPassword.Text))
            {
                mensaje = mensaje + " Debe ingresar su contraseña*";
            }
            if (mensaje == string.Empty && !string.IsNullOrEmpty(txtEmail.Text) && !string.IsNullOrEmpty(txtPassword.Text))
            {
                dtUsuario = ClsUsuario.Busca_Usuario(txtEmail.Text);
                if (dtUsuario.Rows.Count > 0)
                {
                    if (dtUsuario.Rows[0]["EMAIL"].ToString() == txtEmail.Text && dtUsuario.Rows[0]["CONTRASEÑA"].ToString() == txtPassword.Text)
                    {
                        Session["idUsuario"] = dtUsuario.Rows[0]["idUsuario"].ToString();
                        Response.Redirect("../Principal.aspx");
                    }
                    else
                    {
                        Response.Write("<script language=javascript>alert('" + "Usuario y/o contraseña invalido" + "');</script>");

                    }
                }
                else
                {
                    Response.Write("<script language=javascript>alert('" + "Usuario no encontrado" + "');</script>");
                }
            }
            else
            {
                Response.Write("<script language=javascript>alert('" + mensaje + "');</script>");
            }

        }
    }
}