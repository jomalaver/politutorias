﻿using DataAccess.LogicBusiness;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Threading;
using DayPilot.Utils;
using DayPilot.Web.Ui;
using DayPilot.Web.Ui.Data;
using DayPilot.Web.Ui.Enums;
using DayPilot.Web.Ui.Events;
using DayPilot.Web.Ui.Events.Scheduler;
using System.Web;
using System.Web.UI;

namespace Tutorias.Vistas.Tutorias
{
    public partial class Tutorias : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["idUsuario"] == null)
                    Response.Redirect("../Vistas/Login/Login.aspx");
                if (Convert.ToInt32(DateTime.Now.Day.ToString()) < 15)
                    DPNAgendamiento.StartDate = DateTime.Now.AddMonths(-1);
                CargaAgendamiento();
            }

        }
        protected void CargaAgendamiento()
        {
            LoadResources();
            DPSAgendamiento.DataSource = DbSelectEvents(DPSAgendamiento.StartDate, DPSAgendamiento.EndDate.AddDays(1));
            DPSAgendamiento.ShowToolTip = true;
            DPSAgendamiento.DataBind();
        }
        protected void DPSAgendamiento_Command(object sender, CommandEventArgs e)
        {
            switch (e.Command)
            {
                case "navigate":
                    DPSAgendamiento.StartDate = (DateTime)e.Data["day"];
                    DPSAgendamiento.DataSource = DbSelectEvents(DPSAgendamiento.StartDate, DPSAgendamiento.EndDate.AddDays(1));
                    DPSAgendamiento.DataBind();
                    DPSAgendamiento.Update();
                    break;
                case "refresh":
                    DPSAgendamiento.DataSource = DbSelectEvents(DPSAgendamiento.StartDate, DPSAgendamiento.EndDate.AddDays(1));
                    DPSAgendamiento.DataBind();
                    DPSAgendamiento.Update();
                    break;
            }
        }
        protected void DPSAgendamiento_EventClick(object sender, EventClickEventArgs e)
        {
            Redireccionar(e.Start, e.End, e.Value, e.ResourceId);
        }
        protected void DPSAgendamiento_TimeRangeSelected(object sender, TimeRangeSelectedEventArgs e)
        {
            Redireccionar(e.Start, e.End, "0", e.Resource);
        }
        protected void DayPilotCalendarWeek_OnEventClick(object sender, EventClickEventArgs e)
        {
            Redireccionar(e.Start, e.End, e.Value, e.ResourceId);
        }
        private void Redireccionar(DateTime start, DateTime end, string id_Agenda, string codigo_Recurso)
        {
            Session["Agenda"] = start.ToString("d/M/yyyy HH:mm") + "¬" + end.ToString("d/M/yyyy HH:mm") + "¬" + id_Agenda + "¬" + codigo_Recurso ;
            Response.Redirect("DatosTutoria.aspx");
        }
        private DataTable DbSelectEvents(DateTime start, DateTime end)
        {
            LoadResources();
            DataTable dt = ClsTutoria.LoadTutoria(start, end);
            return dt;

        }
        private void LoadResources()
        {
            DPSAgendamiento.Resources.Clear();
            if (DPSAgendamiento.HeaderColumns.Count == 0)
            {
                DPSAgendamiento.HeaderColumns.Add(new RowHeaderColumn("", 1));
                DPSAgendamiento.HeaderColumns.Add(new RowHeaderColumn("Profesor", 30));
            }
            DataTable dt = ClsTutoria.LoadProfesor(DPSAgendamiento.StartDate);
            foreach (DataRow dr in dt.Rows)
            {
                Resource r = new Resource("", Convert.ToString(dr["ID"]));
                r.Columns.Add(new ResourceColumn((string)dr["NAME"] + "<br>" + "Tel: " + dr["phone"].ToString()));
                r.ToolTip = "Especialidad: " + dr["ESPECIALIDAD"].ToString();
                DPSAgendamiento.Resources.Add(r);
            }
        }
        protected void Timer1_Tick(object sender, EventArgs e)
        {
            CargaAgendamiento();
        }
        protected void DPSAgendamiento_BeforeEventRender(object sender, BeforeEventRenderEventArgs e)
        {
            DataTable dtAgenda = new DataTable();
            if (dtAgenda.Rows.Count > 0)
            {
                e.BubbleHtml = "Inicio :" + (string)e.DataItem["eventstart"].ToString() + "<br />" +
                   "Fin :" + (string)e.DataItem["eventend"].ToString() + "<br />" +
                   "Nombre Agenda :" + (string)e.DataItem["name"].ToString() + "<br />" +
                   "Expediente :" + dtAgenda.Rows[0]["nombreTutoria"].ToString() + "<br />";
                if (dtAgenda.Rows[0]["codEstado"].ToString() == "1")
                    e.BackgroundColor = "LawnGreen";
                else if (dtAgenda.Rows[0]["ESTADO_AGENDA"].ToString() == "2")
                    e.BackgroundColor = "CornflowerBlue";
                e.FontColor = "Black";
            }
        }
    }
}
