﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true" CodeBehind="Tutorias.aspx.cs" Inherits="Tutorias.Vistas.Tutorias.Tutorias" %>

<%@ Register Assembly="DayPilot" Namespace="DayPilot.Web.Ui" TagPrefix="DayPilot" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <div id="Tutorias-crear" style="border-bottom: 1px solid #333">
            <h3>Tutorias</h3>
        </div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div style="float: left; width: 150px">
            <asp:HiddenField ID="hfid" runat="server" />
            <DayPilot:DayPilotNavigator ID="DPNAgendamiento" runat="server" ShowMonths="2" SkiptMonth="3"
                BoundDayPilotID="DPSAgendamiento" VisibleRangeChangedHandling="Disabled" WeekStarts="Monday" />
            <asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick"
                Interval="60000">
            </asp:Timer>
        </div>
        <div id="tabs" style="margin-left: 150px;">
            <DayPilot:DayPilotScheduler runat="server" DataEndField="eventend" DataStartField="eventstart"
                DataTextField="name" DataIdField="id" DataResourceField="resource" ID="DPSAgendamiento"
                OnCommand="DPSAgendamiento_Command" TimeRangeSelectedHandling="PostBack" EventClickHandling="PostBack"
                OnEventClick="DPSAgendamiento_EventClick" OnTimeRangeSelected="DPSAgendamiento_TimeRangeSelected"
                BusinessBeginsHour="6" BusinessEndsHour="12" CellDuration="30" CellGroupBy="Day"
                CellWidth="110" BorderWidth="1" CellBorderColor="Gray" EventHeight="25"
                HeaderHeight="25" HourFontSize="7pt" LoadingLabelText="Cargando..."
                RowHeaderWidth="25" LoadingLabelVisible="False" Height="300px"
                Font-Size="6pt" AutoScroll="Drag" CellWidthSpec="Auto"
                EmptyBackColor="#00CC99" EventBackColor="#3366FF" EventBorderColor="Black"
                BubbleID="DayPilotBubble1"
                OnBeforeEventRender="DPSAgendamiento_BeforeEventRender"
                TimeFormat="Clock24Hours" EventFontSize="8pt"
                ResourcesStatic="False" Shadow="Fill" RowMinHeight="31">
                <TimeHeaders>
                    <DayPilot:TimeHeader GroupBy="Day" Format="dddd MMMM d, yyyy" />
                    <DayPilot:TimeHeader GroupBy="Hour" Format="H:mm" />
                </TimeHeaders>
            </DayPilot:DayPilotScheduler>
            <DayPilot:DayPilotBubble ID="DayPilotBubble1" runat="server" CssOnly="true" CssClassPrefix="bubble_default" />
        </div>
    </div>
</asp:Content>
