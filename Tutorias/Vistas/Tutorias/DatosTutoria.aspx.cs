﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Tutorias.Vistas.Tutorias
{
    public partial class DatosTutoria : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["idUsuario"] == null)
                    Response.Redirect("../Vistas/Login/Login.aspx");
                cargaDatos();
            }

        }
        public void cargaDatos()
        {
            string[] values = Session["Agenda"].ToString().Split('¬');
            txtFechaIngreso.Text = values[0];
            txtFechaFin.Text = values[1];
            hdId.Value = values[2];
        }

    }
}