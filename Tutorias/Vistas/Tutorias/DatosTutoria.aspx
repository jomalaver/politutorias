﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true" CodeBehind="DatosTutoria.aspx.cs" Inherits="Tutorias.Vistas.Tutorias.DatosTutoria" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hdId" runat="server" />
    <div class="container-fluid">
        <div id="titulo-crear-Usuario" style="border-bottom: 1px solid #333">
            <h3>Usuario <small>Nuevo</small></h3>
        </div>
        <br />
        <div class="form-group" style="float: right">
        </div>
        <br />
        <div class="form-group">
            <asp:Label runat="server" Text="Nombre Tutoria"></asp:Label>
            <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Fecha Inicio"></asp:Label>
            <asp:TextBox ID="txtFechaIngreso" runat="server" TextMode="Date" placeholder="12/05/1800" CssClass="form-control" Enabled="false"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Fecha Fin"></asp:Label>
            <asp:TextBox ID="txtFechaFin" runat="server" TextMode="Date" placeholder="12/05/1800" CssClass="form-control" Enabled="false"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Profesor "></asp:Label>
            <asp:DropDownList ID="ddlProfesor" runat="server" CssClass="form-control"></asp:DropDownList>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text=" Estudiante"></asp:Label>
            <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
        </div>

        <div class="form-group">
            <asp:CustomValidator ID="cvError" Text="*" runat="server" CssClass="text-danger" Display="None" ValidationGroup="validarFormulario">                                </asp:CustomValidator>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="text-danger" ValidationGroup="validarFormulario" />
            <asp:Label ID="Label1" runat="server" Text="" CssClass="text-danger"></asp:Label>
        </div>
        <div class="form-group">
            <asp:Button ID="btnCrear" runat="server" Text="Confirmar" CssClass="btn btn-default" />
            <asp:Button ID="btnIndice" runat="server" Text="Atras" CssClass="btn btn-danger" />
        </div>

        <div class="alert alert-danger" runat="server" id="alertError">
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
        </div>
        <div class="alert alert-success" runat="server" id="alertSucess">
            <asp:Label ID="lblSucess" runat="server" Text=""></asp:Label>
        </div>
    </div>
</asp:Content>
