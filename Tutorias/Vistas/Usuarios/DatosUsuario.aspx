﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true" CodeBehind="DatosUsuario.aspx.cs" Inherits="Tutorias.Vistas.Usuarios.DatosUsuario" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <div id="titulo-crear-Usuario" style="border-bottom: 1px solid #333">
            <h3>Usuario <small>Nuevo</small></h3>
        </div>
        <br />
        <div class="form-group" style="float: right">
        </div>
        <br />
        <div class="form-group">
            <asp:Label runat="server" Text="Correo "></asp:Label>
            <asp:TextBox ID="txtCorreo" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Contraseña "></asp:Label>
            <asp:TextBox ID="txtContraseña" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Nombres"></asp:Label>
            <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Apellidos"></asp:Label>
            <asp:TextBox ID="txtApellidos" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Tipo Documento"></asp:Label>
            <asp:DropDownList ID="ddlTipoDocumento" runat="server" CssClass="form-control"></asp:DropDownList>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Numero De Identificacion"></asp:Label>
            <asp:TextBox ID="txtIdentificacion" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Direccion"></asp:Label>
            <asp:TextBox ID="txtDireccion" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Telefono"></asp:Label>
            <asp:TextBox ID="txtTelefono" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Fecha Ingreso"></asp:Label>
            <asp:TextBox ID="txtFechaIngreso" runat="server" TextMode="Date" placeholder="12/05/1800" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Ciudad"></asp:Label>
            <asp:DropDownList ID="ddlCiudad" runat="server" CssClass="form-control"></asp:DropDownList>
        </div>
        <div class="form-group">
            <asp:Label runat="server" Text="Rol"></asp:Label>
            <asp:DropDownList ID="ddlRol" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlRol_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
        </div>
        <asp:Panel ID="pnlDisponibilidad" runat="server" Visible="false">

            <div class="form-group">
                <asp:Label runat="server" Text="Disponibilidad"></asp:Label>
                <asp:DropDownList ID="ddlDisponibilidad" runat="server" CssClass="form-control"></asp:DropDownList>
            </div>
            <div class="form-group">
                <asp:Label runat="server" Text="Especialidad"></asp:Label>
                <asp:DropDownList ID="ddlEspecialidad" runat="server" CssClass="form-control"></asp:DropDownList>
            </div>
        </asp:Panel>
        <div class="form-group">
            <asp:CustomValidator ID="cvError" Text="*" runat="server" CssClass="text-danger" Display="None" ValidationGroup="validarFormulario">                                </asp:CustomValidator>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="text-danger" ValidationGroup="validarFormulario" />
            <asp:Label ID="Label1" runat="server" Text="" CssClass="text-danger"></asp:Label>
        </div>
        <div class="form-group">
            <asp:Button ID="btnCrear" runat="server" Text="Crear" CssClass="btn btn-default" OnClick="btnCrear_Click" />
            <asp:Button ID="btnIndice" runat="server" Text="Atras" CssClass="btn btn-danger" />
        </div>

        <div class="alert alert-danger" runat="server" id="alertError">
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
        </div>
        <div class="alert alert-success" runat="server" id="alertSucess">
            <asp:Label ID="lblSucess" runat="server" Text=""></asp:Label>
        </div>
    </div>
</asp:Content>
