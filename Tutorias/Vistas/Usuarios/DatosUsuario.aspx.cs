﻿using DataAccess.LogicBusiness;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Tutorias.Vistas.Usuarios
{
    public partial class DatosUsuario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["idUsuario"] == null)
                    Response.Redirect("../Login/Login.aspx");
                alertError.Attributes.Add("Style", "Display:none");
                alertSucess.Attributes.Add("Style", "Display:none");
                CargaDatos();
            }

        }

        public void CargaDatos()
        {
            DataTable dtUsuario = ClsUsuario.Usuarios(Session["idUsuario"].ToString());
            Help.FillddlCiudad(ddlCiudad);
            Help.FillddlDisponibilidad(ddlDisponibilidad);
            Help.FillddlRol(ddlRol);
            Help.FillddlTipoDocumentos(ddlTipoDocumento);
            Help.FillddlEspecialidad(ddlEspecialidad);
            txtNombre.Text = dtUsuario.Rows[0]["nombres"].ToString();
            txtApellidos.Text = dtUsuario.Rows[0]["apellidos"].ToString();
            ddlTipoDocumento.SelectedValue = dtUsuario.Rows[0]["codDocumento"].ToString();
            txtIdentificacion.Text = dtUsuario.Rows[0]["numeroIdentificacion"].ToString();
            txtIdentificacion.Text = dtUsuario.Rows[0]["direccion"].ToString();
            txtTelefono.Text = dtUsuario.Rows[0]["telefono"].ToString();
            txtCorreo.Text = dtUsuario.Rows[0]["email"].ToString();
            txtFechaIngreso.Text = dtUsuario.Rows[0]["fechaIngreso"].ToString();
            ddlCiudad.SelectedValue = dtUsuario.Rows[0]["CodCiudad"].ToString();
            ddlRol.SelectedValue = dtUsuario.Rows[0]["codRol"].ToString();
            txtContraseña.Text = dtUsuario.Rows[0]["contraseña"].ToString();
        }
        protected void btnCrear_Click(object sender, EventArgs e)
        {
            int idUsuario = 0;
            if (Session["idUsuario"] != null)
                idUsuario = Convert.ToInt32(Session["idUsuario"].ToString());
            ClsUsuario.InsertaUsuario(
                txtNombre.Text, txtApellidos.Text, ddlTipoDocumento.SelectedValue, txtIdentificacion.Text,
                txtDireccion.Text, txtTelefono.Text, txtCorreo.Text, Convert.ToDateTime(txtFechaIngreso.Text), ddlCiudad.SelectedValue,
                ddlRol.SelectedValue, ddlDisponibilidad.SelectedValue, ddlEspecialidad.SelectedValue, "1", txtContraseña.Text, idUsuario);
        }
        protected void ddlRol_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlRol.SelectedValue == "1")
            {
                pnlDisponibilidad.Visible = true;
            }
            if (ddlRol.SelectedValue == "2")
            {
                pnlDisponibilidad.Visible = false;
            }
        }
    }
}