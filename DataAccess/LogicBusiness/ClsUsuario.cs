﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.DataAccess;

namespace DataAccess.LogicBusiness
{
    public class ClsUsuario
    {
        public static void Connection(clsConnection objConnexion)
        {
            string strCadenaConexion = System.Configuration.ConfigurationManager.AppSettings["BD"];
            objConnexion.ConnectionString(strCadenaConexion);
        }
        public static DataTable Busca_Usuario(string usuario)
        {
            DataTable dtDatos;
            clsConnection objConnexion = new clsConnection();
            Connection(objConnexion);
            objConnexion.NameProcedure("RASP_LOAD_USUARIO");
            objConnexion.Parameters("@pemail", usuario);
            dtDatos = (DataTable)objConnexion.ExecuteProc((int)clsConnection.TipoDato.Table);
            objConnexion.DisposeProc();
            return dtDatos;
        }

        public static DataTable Usuarios(string idUsuario)
        {
            DataTable dtDatos;
            clsConnection objConnexion = new clsConnection();
            Connection(objConnexion);
            objConnexion.NameProcedure("RASP_LOAD_USUARIOS");
            objConnexion.Parameters("@IDUSUARIO", idUsuario);
            dtDatos = (DataTable)objConnexion.ExecuteProc((int)clsConnection.TipoDato.Table);
            objConnexion.DisposeProc();
            return dtDatos;
        }
        public static int InsertaUsuario(
            string nombres, string apellidos, string codDocumento
           , string numeroIdentificacion, string direccion, string telefono, string email
           , DateTime fechaIngreso, string CodCiudad, string codRol, string codDisponibilidad,
            string codEspecialidad, string codEstado, string contraseña, int idUsuario = 0)
        {
            try
            {
                clsConnection objConnexion = new clsConnection();
                Connection(objConnexion);
                objConnexion.NameProcedure("RASP_INSERTA_USUARIO");
                objConnexion.Parameters("@IdUsuario", idUsuario);
                objConnexion.Parameters("@nombres", nombres);
                objConnexion.Parameters("@apellidos", apellidos);
                objConnexion.Parameters("@codDocumento", codDocumento);
                objConnexion.Parameters("@numeroIdentificacion", numeroIdentificacion);
                objConnexion.Parameters("@direccion", direccion);
                objConnexion.Parameters("@telefono", telefono);
                objConnexion.Parameters("@email", email);
                objConnexion.Parameters("@fechaIngreso", fechaIngreso);
                objConnexion.Parameters("@CodCiudad", CodCiudad);
                objConnexion.Parameters("@codRol", codRol);
                objConnexion.Parameters("@codDisponibilidad", codDisponibilidad);
                objConnexion.Parameters("@codEspecialidad", codEspecialidad);
                objConnexion.Parameters("@codEstado", codEstado);
                objConnexion.Parameters("@contraseña", contraseña);

                DataTable dtAgenda = (DataTable)objConnexion.ExecuteProc((int)clsConnection.TipoDato.Table);
                objConnexion.DisposeProc();
                if (dtAgenda.Rows.Count > 0)
                    return Convert.ToInt32(dtAgenda.Rows[0]["AGENDA"].ToString());
                else
                    return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0} First exception caught.", ex);
                throw new Exception(ex.Message);
            }
        }
    }
}
