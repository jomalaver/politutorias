﻿using DataAccess.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace DataAccess.LogicBusiness
{
    public class Help
    {
        public static void Connection(clsConnection objConnexion)
        {
            string strCadenaConexion = System.Configuration.ConfigurationManager.AppSettings["BD"];
            objConnexion.ConnectionString(strCadenaConexion);
        }
        public static void FillddlTipoDocumentos(DropDownList lista)
        {
            DataTable dtDatos = new DataTable();
            clsConnection objConnexion = new clsConnection();
            Connection(objConnexion);
            objConnexion.NameProcedure("rasp_load_tipo_documento");
            dtDatos = (DataTable)objConnexion.ExecuteProc((int)clsConnection.TipoDato.Table);
            objConnexion.DisposeProc();
            ListaLlenar(lista, dtDatos);
        }
        public static void FillddlCiudad(DropDownList lista)
        {
            DataTable dtDatos = new DataTable();
            clsConnection objConnexion = new clsConnection();
            Connection(objConnexion);
            objConnexion.NameProcedure("RASP_LOAD_CIUDAD");
            dtDatos = (DataTable)objConnexion.ExecuteProc((int)clsConnection.TipoDato.Table);
            objConnexion.DisposeProc();
            ListaLlenar(lista, dtDatos);
        }
        public static void FillddlRol(DropDownList lista)
        {
            DataTable dtDatos = new DataTable();
            clsConnection objConnexion = new clsConnection();
            Connection(objConnexion);
            objConnexion.NameProcedure("RASP_LOAD_ROL");
            dtDatos = (DataTable)objConnexion.ExecuteProc((int)clsConnection.TipoDato.Table);
            objConnexion.DisposeProc();
            ListaLlenar(lista, dtDatos);
        }
        public static void FillddlDisponibilidad(DropDownList lista)
        {
            DataTable dtDatos = new DataTable();
            clsConnection objConnexion = new clsConnection();
            Connection(objConnexion);
            objConnexion.NameProcedure("RASP_LOAD_DISPONIBILIDAD");
            dtDatos = (DataTable)objConnexion.ExecuteProc((int)clsConnection.TipoDato.Table);
            objConnexion.DisposeProc();
            ListaLlenar(lista, dtDatos);
        }
        public static void FillddlEspecialidad(DropDownList lista)
        {
            DataTable dtDatos = new DataTable();
            clsConnection objConnexion = new clsConnection();
            Connection(objConnexion);
            objConnexion.NameProcedure("RASP_LOAD_ESPECIALIDAD");
            dtDatos = (DataTable)objConnexion.ExecuteProc((int)clsConnection.TipoDato.Table);
            objConnexion.DisposeProc();
            ListaLlenar(lista, dtDatos);
        }
        private static void ListaLlenar(DropDownList lista, DataTable dtDatos)
        {
            lista.Items.Clear();
            ListItem item = new ListItem(".:Seleccionar:.", "0");
            lista.Items.Insert(0, item);
            if (dtDatos != null)
            {
                foreach (DataRow r in dtDatos.Rows)
                {
                    ListItem it = new ListItem(r[0].ToString(), r[1].ToString());
                    lista.Items.Add(it);
                }
            }
            if (lista.Items.Count == 2)
            {
                lista.SelectedIndex = 1;
            }
            else
            {
                lista.SelectedIndex = 0;
            }
        }
    }
}
