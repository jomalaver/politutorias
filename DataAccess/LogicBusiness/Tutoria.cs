﻿using DataAccess.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.LogicBusiness
{
    public class Tutoria
    {
        public static void Connection(clsConnection objConnexion)
        {
            string strCadenaConexion = System.Configuration.ConfigurationManager.AppSettings["BD"];
            objConnexion.ConnectionString(strCadenaConexion);
        }
        public static DataTable LoadProfesor(DateTime fechaTutoria)
        {
            DataTable dtDatos;
            clsConnection objConnexion = new clsConnection();
            Connection(objConnexion);
            objConnexion.NameProcedure("RASP_LOAD_PROFESOR");
          //  objConnexion.Parameters("", );
            dtDatos = (DataTable)objConnexion.ExecuteProc((int)clsConnection.TipoDato.Table);
            objConnexion.DisposeProc();
            return dtDatos;
        }
        public static DataTable LoadTutoria(DateTime fechaInicio, DateTime fechaFin)
        {
            DataTable dtDatos;
            clsConnection objConnexion = new clsConnection();
            Connection(objConnexion);
            objConnexion.NameProcedure("RASP_LOAD_TUTORIA");
            objConnexion.Parameters("@FECHA_INI", fechaInicio);
            objConnexion.Parameters("@FECHA_FIN", fechaFin);
            dtDatos = (DataTable)objConnexion.ExecuteProc((int)clsConnection.TipoDato.Table);
            objConnexion.DisposeProc();
            return dtDatos;
        }
    }
}
