﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.DataAccess
{
    public class clsConnection
    {
        private string mstrConnectionString;
        private string mstrNameProcedure;
        private SqlCommand mcmTemp;
        private SqlDataReader myReader; // Para recoger los valores de la consulta de los parametros
        private SqlCommand CommandParam; // Para consultar los parametros del procedimiento almacenado
        private SqlCommand CommandSP; // Para ejecutar el procedimiento almacenado			
        private SqlParameter workParm; // Para los parametros de salida de los procedimientos almacenados
        private SqlDataAdapter mySqlDataAdapter; // Para rellenar el dataset con los resultados de la consulta


        public enum TipoDato : int
        {
            Table = 1,
            View = 2,
            DataSet = 3,
            DataReader = 4
        }

        public clsConnection()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public void ConnectionString(string value)
        {
            mstrConnectionString = value;
        }

        public void NameProcedure(string value)
        {
            mstrNameProcedure = value;
            StoredProcedure();
        }

        private void StoredProcedure()
        {
            DataTable dttParameters = new DataTable();
            SqlConnection cnCon = new SqlConnection(mstrConnectionString);
            SqlCommand cmCom = new SqlCommand("sp_sproc_columns", cnCon);
            SqlDataAdapter dtaAd = new SqlDataAdapter();
            mcmTemp = new SqlCommand();
            ConnectionMananger.OpenConection();
            cmCom.CommandType = CommandType.StoredProcedure;
            cmCom.Parameters.Add(new SqlParameter("@procedure_name", SqlDbType.NVarChar, 390));
            cmCom.Parameters["@procedure_name"].Value = mstrNameProcedure;
            dtaAd.SelectCommand = cmCom;
            dtaAd.Fill(dttParameters);
            for (int i = 0; i < dttParameters.Rows.Count; i++)
            {
                switch (Convert.ToInt32((dttParameters.Rows[i]["COLUMN_TYPE"]).ToString()))
                {
                    case 1: mcmTemp.Parameters.Add(dttParameters.Rows[i]["COLUMN_NAME"].ToString(), TipoDatoBD(dttParameters.Rows[i]["TYPE_NAME"].ToString()), Convert.ToInt32(dttParameters.Rows[i]["PRECISION"].ToString())); break;
                    case 2:
                        mcmTemp.Parameters.Add(dttParameters.Rows[i]["COLUMN_NAME"].ToString(), TipoDatoBD(dttParameters.Rows[i]["TYPE_NAME"].ToString()), Convert.ToInt32(dttParameters.Rows[i]["PRECISION"].ToString()));
                        mcmTemp.Parameters[Convert.ToInt32(dttParameters.Rows[i]["COLUMN_NAME"].ToString())].Direction = ParameterDirection.Output; break;
                    default: break;
                }
            }
            ConnectionMananger.CloseConnection();
        }

        public void Parameters(string strNameParameter, string strValor)
        {
            if (strValor == null)
                Parameters(strNameParameter, DBNull.Value);
            else
                mcmTemp.Parameters[strNameParameter].Value = strValor;
        }

        public void Parameters(int intIndex, string strValor)
        {
            if (strValor == null)
                Parameters(intIndex, DBNull.Value);
            else
                mcmTemp.Parameters[intIndex].Value = strValor;
        }

        public void Parameters(string strNameParameter, object objValor)
        {
            try
            {
                mcmTemp.Parameters[strNameParameter].Value = objValor == null ? DBNull.Value : objValor;
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
        }

        public void Parameters(int intIndex, object objValor)
        {
            mcmTemp.Parameters[intIndex].Value = objValor == null ? DBNull.Value : objValor;
        }
        /// Metodo que Carga los parametros del Procedimiento Almacenado
        /// Genera todos los Parametros de Entrada y Salida del procedimiento
        /// </summary>
        /// <param name="Parameters">Numero indeterminado de Parametros del procedimiento almacenado</param>
        private void CargaFabrica(params object[] Parameters)
        {
            CommandParam = new SqlCommand("Select syscolumns.name,syscolumns.xtype,syscolumns.prec,syscolumns.isoutparam from syscolumns,sysobjects where (sysobjects.id=syscolumns.id) and (sysobjects.name='" + mstrNameProcedure + "')", ConnectionMananger.OpenConection());

            myReader = CommandParam.ExecuteReader();

            // Llamo al procedimiento almacenado pasado como parametro
            CommandSP = new SqlCommand(mstrNameProcedure, ConnectionMananger.OpenConection());
            CommandSP.CommandType = CommandType.StoredProcedure;

            // bucle para generar todos los parametros
            int counter = 0;
            while (myReader.Read())
            {
                if (myReader.GetInt32(3) != 1) // Si el parametro es de entrada
                {
                    if (Parameters[counter].ToString() != "")
                    {
                        CommandSP.Parameters.Add(myReader.GetValue(0).ToString(), DataType(myReader.GetByte(1)), myReader.GetInt16(2));
                        CommandSP.Parameters[myReader.GetValue(0).ToString()].Value = ConvertDataType(myReader.GetByte(1), Parameters[counter].ToString());
                    }
                    else
                    {
                        CommandSP.Parameters.Add(myReader.GetValue(0).ToString(), DataType(myReader.GetByte(1)), myReader.GetInt16(2));
                        CommandSP.Parameters[myReader.GetValue(0).ToString()].Value = DBNull.Value;
                    }
                }
                else // si el parametro es de salida
                {
                    workParm = CommandSP.Parameters.Add(myReader.GetValue(0).ToString(), DataType(myReader.GetByte(1)), myReader.GetInt16(2));
                    workParm.Direction = ParameterDirection.Output;
                }

                counter++;
            }
            myReader.Close();
        }

        public void ExecuteProc()
        {
            SqlConnection cnTemp = new SqlConnection(mstrConnectionString);

            try
            {
                mcmTemp.Connection = cnTemp;
                ConnectionMananger.OpenConection();
                mcmTemp.CommandText = mstrNameProcedure;
                mcmTemp.CommandType = CommandType.StoredProcedure;
                mcmTemp.CommandTimeout = 5000;
                mcmTemp.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                System.Console.Write(ex);
            }
            finally
            {
                ConnectionMananger.CloseConnection();
            }
        }

        public Object ExecuteProc(int eTipoDato)
        {
            SqlDataAdapter dtaTemp = new SqlDataAdapter();
            DataTable dttTemp = new DataTable();
            DataSet dtsTemp = new DataSet();
            DataView dtvTemp = new DataView();
            SqlDataReader dtrTemp;
            SqlConnection cnTemp = new SqlConnection(mstrConnectionString);
            try
            {
                mcmTemp.Connection = cnTemp;
                ConnectionMananger.OpenConection();
                mcmTemp.CommandText = mstrNameProcedure;
                mcmTemp.CommandType = CommandType.StoredProcedure;
                mcmTemp.CommandTimeout = 5000;
                dtaTemp.SelectCommand = mcmTemp;
                switch (eTipoDato)
                {
                    case (int)TipoDato.Table:
                        dtaTemp.Fill(dttTemp);
                        return dttTemp;
                    case (int)TipoDato.View:
                        dtaTemp.Fill(dttTemp);
                        dttTemp.TableName = "Temp";
                        dtvTemp.Table = dttTemp;
                        return dtvTemp;
                    case (int)TipoDato.DataSet:
                        dtaTemp.Fill(dtsTemp, "Temp");
                        return dtsTemp;
                    case (int)TipoDato.DataReader:
                        dtrTemp = mcmTemp.ExecuteReader();
                        return dtrTemp;
                    default: return null;
                }
            }
            catch (Exception ex)
            {
                System.Console.Write(ex);
                return null;
            }
            finally
            {
                ConnectionMananger.CloseConnection();
            }
        }
        /// <summary>
        /// Ejecuta un procedimiento almacenado que contiene una consulta de actualizacion
        /// </summary>
        /// <param name="Procedure">Nombre del procedimiento almacenado</param>
        /// <param name="Parameters">Numero indeterminado de Parametros del procedimiento almacenado</param>
        /// <returns>
        /// Devuelve true si el procedimiento almacenado se ha ejecutado con exito
        /// o false en caso contrario
        /// </returns>
        public Boolean ExecuteSpNonQuery(params object[] Parameters)
        {
            Boolean result; // Para devolver la respuesta        	

            try
            {
                CargaFabrica(Parameters);
                // ejecuto el procedimiento almacenado
                result = Convert.ToBoolean(CommandSP.ExecuteNonQuery());

            }
            catch (SqlException ex)
            {
                string comment = "";
                comment = ex.Source;
                result = false;
            }
            catch (Exception e)
            {
                // realiza un control de errores guardando el error que
                // se ha producido en el visor de sucesos
                string comment = "";
                comment = e.Source;
                comment += ". " + e.Message;
                comment += ". " + e.StackTrace;

                result = false;
            }
            finally
            {
                ConnectionMananger.CloseConnection();
            }

            return result;
        }
        public int ExecuteNonQuery(string strSQL)
        {
            SqlConnection cnConexion = new SqlConnection(mstrConnectionString);
            SqlCommand cmComando = new SqlCommand(strSQL, cnConexion);
            ConnectionMananger.OpenConection();
            ConnectionMananger.CloseConnection();
            cmComando.Dispose();
            return cmComando.ExecuteNonQuery();
        }

        public void DisposeProc()
        {
            mcmTemp.Dispose();
        }

        private SqlDbType TipoDatoBD(string strNameType)
        {
            if (strNameType == "bigint")
                return SqlDbType.BigInt;
            if (strNameType == "binary")
                return SqlDbType.Binary;
            if (strNameType == "bit")
                return SqlDbType.Bit;
            if (strNameType == "char")
                return SqlDbType.Char;
            if (strNameType == "datetime")
                return SqlDbType.DateTime;
            if (strNameType == "numeric")
                return SqlDbType.Int;
            if (strNameType == "decimal")
                return SqlDbType.Decimal;
            if (strNameType == "float")
                return SqlDbType.Float;
            if (strNameType == "image")
                return SqlDbType.Image;
            if (strNameType == "int")
                return SqlDbType.Int;
            if (strNameType == "money")
                return SqlDbType.Money;
            if (strNameType == "nchar")
                return SqlDbType.NChar;
            if (strNameType == "ntext")
                return SqlDbType.NText;
            if (strNameType == "nvarchar")
                return SqlDbType.NVarChar;
            if (strNameType == "real")
                return SqlDbType.Real;
            if (strNameType == "smalldatetime")
                return SqlDbType.SmallDateTime;
            if (strNameType == "smallint")
                return SqlDbType.SmallInt;
            if (strNameType == "smallmoney")
                return SqlDbType.SmallMoney;
            if (strNameType == "text")
                return SqlDbType.Text;
            if (strNameType == "timestamp")
                return SqlDbType.Timestamp;
            if (strNameType == "tinyint")
                return SqlDbType.TinyInt;
            if (strNameType == "uniqueidentifier")
                return SqlDbType.UniqueIdentifier;
            if (strNameType == "varbinary")
                return SqlDbType.VarBinary;
            if (strNameType == "varchar")
                return SqlDbType.VarChar;
            return SqlDbType.Variant;
        }
        /// <summary>
        /// Determina el tipo de datos del parametro
        /// </summary>
        /// <param name="IdType">Identificador numerico del tipo de datos en Sql Server</param>
        /// <returns>Devuelve un tipo de datos de Sql Server</returns>
        private SqlDbType DataType(int IdType)
        {
            SqlDbType type = new SqlDbType();

            switch (IdType)
            {
                case 127: // bigint					
                    type = SqlDbType.BigInt;
                    break;
                case 104: // bit 
                    type = SqlDbType.Bit;
                    break;
                case 175: // char
                    type = SqlDbType.Char;
                    break;
                case 61:  // datetime
                    type = SqlDbType.DateTime;
                    break;
                case 106:  // decimal
                    type = SqlDbType.Decimal;
                    break;
                case 62:  // float
                    type = SqlDbType.Float;
                    break;
                case 56:  // int
                    type = SqlDbType.Int;
                    break;
                case 60:  // money
                    type = SqlDbType.Money;
                    break;
                case 239:  // nchar
                    type = SqlDbType.NChar;
                    break;
                case 99:  // ntext
                    type = SqlDbType.NText;
                    break;
                case 231:  // nvarchar
                    type = SqlDbType.NVarChar;
                    break;
                case 59: // real
                    type = SqlDbType.Real;
                    break;
                case 58:  // smalldatetime
                    type = SqlDbType.SmallDateTime;
                    break;
                case 52:  // smallint
                    type = SqlDbType.SmallInt;
                    break;
                case 122:  // smallmoney
                    type = SqlDbType.SmallMoney;
                    break;
                case 98:  // sql_variant
                    type = SqlDbType.Variant;
                    break;
                case 35:  // text
                    type = SqlDbType.Text;
                    break;
                case 189:  // timestamp
                    type = SqlDbType.Timestamp;
                    break;
                case 48:  // tinyint
                    type = SqlDbType.TinyInt;
                    break;
                case 167:  // varchar
                    type = SqlDbType.VarChar;
                    break;

                default:
                    type = SqlDbType.Variant;
                    break;
            }

            return type;
        }
        /// <summary>
        /// Convierte el string al tipo de datos que se corresponda 
        /// con el valor del parametro
        /// </summary>
        /// <param name="IdType">Tipo de datos del parametro</param>
        /// <param name="strValue">String con el valor que debe tener el parametro</param>
        /// <returns>Devuelve el valor con el tipo de datos propio del parametro</returns>
        private object ConvertDataType(int IdType, string strValue)
        {
            object type = new object();

            switch (IdType)
            {
                case 127: // bigint					
                    type = Int64.Parse(strValue);
                    break;
                case 104: // bit 
                    type = Boolean.Parse(strValue);
                    break;
                case 175: // char
                    type = strValue;
                    break;
                case 61:  // datetime
                    // para poner la fecha en formato dd/mm/aaaa
                    CultureInfo myculture = new CultureInfo("es-ES");
                    DateTime mydatetime = DateTime.ParseExact(strValue, "d", myculture);
                    type = mydatetime;

                    break;
                case 106:  // decimal
                    type = Decimal.Parse(strValue);
                    break;
                case 62:  // float
                    type = Double.Parse(strValue);
                    break;
                case 56:  // int
                    type = Int32.Parse(strValue);
                    break;
                case 60:  // money
                    type = Decimal.Parse(strValue);
                    break;
                case 239:  // nchar
                    type = strValue;
                    break;
                case 99:  // ntext
                    type = strValue;
                    break;
                case 231:  // nvarchar
                    type = strValue;
                    break;
                case 59: // real
                    type = Single.Parse(strValue);
                    break;
                case 58:  // smalldatetime
                    type = DateTime.Parse(strValue).ToShortDateString();
                    break;
                case 52:  // smallint
                    type = Int16.Parse(strValue);
                    break;
                case 122:  // smallmoney
                    type = Decimal.Parse(strValue);
                    break;
                case 98:  // sql_variant
                    type = strValue;
                    break;
                case 35:  // text
                    type = strValue;
                    break;
                case 189:  // timestamp
                    type = DateTime.Parse(strValue).ToShortDateString();
                    break;
                case 48:  // tinyint
                    type = Byte.Parse(strValue);
                    break;
                case 167:  // varchar
                    type = strValue;
                    break;
                default:
                    type = strValue;
                    break;
            }
            return type;
        }
    }
}
