﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.DataAccess
{
    internal sealed class ConnectionMananger
    {
        private static string conectionString = ConfigurationManager.AppSettings["BD"];
        private static SqlConnection conection = new SqlConnection(conectionString);
        public static SqlConnection OpenConection()
        {
            try
            {
                using (SqlConnection conexion = new SqlConnection(conectionString))
                {
                    using (SqlCommand comando = new SqlCommand("", conexion))
                    {
                        conexion.Open();
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
            return conection;
        }

        public static SqlConnection CloseConnection()
        {
            try
            {
                conection.Close();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
            return conection;
        }
    }
}
